from django.shortcuts import render
import datetime
from datetime import timedelta
from django.http import HttpResponse

# Create your views here.

def home(request):
    return render(request, 'home.html')

def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s. <h3>gunakan /time/angka untuk menunjukkan rentang waktu berbeda</h3></body></html>" % now
    return HttpResponse(html)

def not_current_datetime(request, angka):
    now = datetime.datetime.now() + timedelta(hours=int(angka))
    html = "<html><body>It is now %s. <h3>gunakan /time/angka untuk menunjukkan rentang waktu berbeda</h3></body></html>" % now
    return HttpResponse(html)