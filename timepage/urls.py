from django.urls import path
from . import views

app_name = 'timepage'

urlpatterns = [
    path('', views.current_datetime, name='home'),
    path('time/<str:angka>/', views.not_current_datetime, name='home2')
]